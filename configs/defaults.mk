# This file is loaded before the one given via CONFIG

## Customization variables
## Change these as you need in your config

ifndef ROOT_DIR
$(error ROOT_DIR is not set)
endif

export JAVA_HOME=/usr/lib/jvm/java-8-oracle/

# delete undesired buildsystems in custom config
export BUILDSYSTEMS=\
gradle \
maven \
buildr \
ant_ivy \
buck \
leiningen \
sbt \
bazel \
pants

export DEFAULT_CONFIG=$(ROOT_DIR)/configs/generated_minimal.mk

# for jinja2 templates
export FILE_NUM=0
export SUBPROJECT_NUM=0

export ANT_DEFAULT_VERSION=1.10.8
export IVY_DEFAULT_VERSION=2.5.0
export MAVEN_DEFAULT_VERSION=3.6.3
export GRADLE_DEFAULT_VERSION=6.5.1
export BUILDR_DEFAULT_VERSION=1.5.3

export LEININGEN_DEFAULT_VERSION=2.9.1
export SBT_DEFAULT_VERSION=1.3.13

# several pants version between 1.4.0 and 1.10.0 have python3 dependencies issues
export PANTS_DEFAULT_VERSION=1.30.0
# versions < 2.0.0 require different build files
export BAZEL_DEFAULT_VERSION=3.4.0
# git repository tag
export BUCK_DEFAULT_VERSION=v2020.06.29.01

## Build orchestration variables
## Should usually be left alone


# where generated sources go and buildsystems are invoked
export BUILD_DIR=$(ROOT_DIR)/build

export RESULTS_DIR=$(BUILD_DIR)/results

# folder containing source resources except for buildfiles
export GENERATED_SOURCES_DIR=$(BUILD_DIR)/buildsrc
export TEMPLATES_DIR=$(ROOT_DIR)/generators
export BUILDSYSTEMS_DIR=$(ROOT_DIR)/buildsystems
export BUILDTEMPLATES_DIR=$(ROOT_DIR)/buildtemplates


# Python helpers
export SCRIPTS_DIR=$(ROOT_DIR)/scripts
# location to drop anything not to be cleaned by "make clean"
export CACHE_DIR=$(ROOT_DIR)/caches

# TARGET_NAME will be defined later...
export CONFIGURED_BUILD_ROOT=$(BUILD_DIR)/$(TARGET_NAME)
export CONFIGURED_BUILD_SOURCE=$(CONFIGURED_BUILD_ROOT)/source
