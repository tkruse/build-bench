# build-bench

A Benchmark setup for Java buildsystems.

See Github pages at: http://tkruse.github.io/build-bench

[![Build Status](https://travis-ci.org/tkruse/build-bench.svg)](https://travis-ci.org/tkruse/build-bench)

The different buildsystems are installed locally on demand by the makefiles.

Buildsystems:

* Apache Ant  (<https://ant.apache.org>)
* Gradle  (<https://gradle.org>)
* Apache Maven  (<https://maven.apache.org>)
* Apache Buildr (<https://buildr.apache.org>)
* Sbt  (<https://www.scala-sbt.org>)
* Leiningen  (<https://leiningen.org>)
* Buck  (<https://buckbuild.com>)
* Pants  (<https://pantsbuild.github.io>)
* Bazel  (<http://bazel.io>)

Also see [my notes](Buildsystems.md)

## Running

```bash
# to run all buildsystems
$ make

# to run all buildsystems freshly (TODO: do something about bazel global cache)
$ rm -rf caches/bazel/bazel<version>/bazel_cache
$ make clean all

# to run for just selected buildsystems, e.g. maven vs. gradle:
$ make clean maven gradle

# to run for just selected buildsystems in particular versions
$ make clean maven3.3.3 gradle2.7

# to run with changed code:
# change file in build/<project>/source
$ touch build/<project>/source
$ make ...
```

The process is configured using variables that can be changed, the configs folder has a `defauls.mk` file setting defaults, and some example files for different kinds of builds.

It is possible to run a custom Benchmark configuration using:

```bash
# to run specific configuration
$ make clean all CONFIG=configs/generated_multi.mk
```

## Prerequisites

* Linux           (MacOS, Unix and BSD may also work)
* Java            (7 or 8, configure JAVA_HOME)
* GNU make        (should be present on any *nix)
* Python          (2.7)
* jinja2          (python library, if using templated sources, install with apt-get on ubuntu. TODO: install locally)
* watchman        (optional python library, optimizes buck. TODO: install locally)
* Ruby            (1 or 2, for Apache buildr, jruby should also work)
* Rubygems        (1.4, for Apache buildr)
* GLIBC, GLIBCXX  (to build bazel)

The whole setup is described [here](Design.md)

## Configuring Benchmarks

Custom configurations are loaded after the `defaults.mk` providing some convention over configuration. If present, a `custom.mk` in the project root will be loaded after `default.mk` but before specific configuration, allowing to override permanent defaults with your defaults.

## Motivation

While Maven and Gradle are used by most Java projects in the wild, there are many alternatives to choose from. Comparing those is difficult. This project is a setup to run a buildprocess for java projects using multiple buildsystems.

My main goal was to find out which buildsystem feature pays off under which circumstances (and under which it is irrelevant). As secondary independent goal is to get a feel for how much difference there is in performance for "realistic" isolated projects (like open-source projects). A comparison for huge mono-repo situations was not that interesting to me.

A single benchmark is driven by GNU make. The `Makefile` creates a `build` folder,
containing a folder structure for benchmarks. Subfolders follow the pattern `<benchmarkname>/<buildsystemname>`. Into those folders, Java source files and buildsystem specific files will be copied / generated. Then the buildsystem is invoked inside that folder and the time until completion is measured.

## Samples

The builds should work for any source tree that follows these conventions:

* Java 8 compliant code
* Java sources in src/main/java
* Test sources in src/test/java
* Other test resources in src/test/resources
* Tests written in JUnit4.11 compatible fashion, not requiring 4.12 (sbt issues)
* Test classes named *Test.java
* Abstract Test parent classes named *AbstractTest.java (to be excluded, some buildsystems fail else)
* Single module projects (as opposed to multi-project builds)
* No other dependencies than standard Java and JUnit4

## Output

Sample output: See http://tkruse.github.io/build-bench/commons-math

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md)

## FAQ

## Which buildsystem should I use?

It depends. See [Buildsystems](Buildsystems.md)

### What influences performance?

JVM startup adds something like 3 seconds to the whole process. Several tools offer daemons to reduce this offset. Tools not written in JVM languages do not have this offset. A background process living on between builds can also cache other valuable data to make further runs startup faster. The startup time reduction however becomes only relevant at large scales, though some people feel annoyed by non-responsive command line tools.

Parallel downloads of dependencies: This usually "only" affects first time builds, but this can have a big impact when building on "clean" CI servers. Recently Pants and Bazel started depending on the new Coursier library to download dependencies in parallel. Other buildsystems just recommend including all dependencies in the version control (with a possible performance impact on some version control operations).

Parallel task execution: On machines with multiple cores, it may be possible to reduce build time by utilizing more than one CPU. Even with just one CPU, multithreaded execution can have a performance bonus. However the build-time rarely is reduced by the number of CPUs. The overhead of finding out how to split tasks over several CPUs can eliminate benefits, and often there will be many dependencies that lead to tasks necessarily being build in sequence. Most buildtool will thus mostly offer to only build completely independent sub-modules in parallel. For single-module projects, no additional CPU is used then.
Some tasks may even only work when not run in parallel, so using parallel features also increases maintenance effort.

Compiler speed may differ for different compilers. The scala compiler and clojure compiler seemed slower than javac for compiling java sources.

Incremental re-compilation, meaning compiling only files that are affected by a change, can drastically reduce build times.

Compilation avoidance extends incremental compilation. This is a new feature in gradle 3.4, where dependent classfiles do not get recompiled when a change in one class file is API compatible with the previous version.

Incremental build steps beyond compilation help (e.g. Maven can compile incrementally, but not test incrementally).

Incremental builds for sub-module filesets. Several buildsystems can (re-)build only those submodules that have change, but cannot only (re-)build only half a submodule. Being able to incrementally rebuild smaller parts can speed up builds in specific situations.

Incremental compilation that understand the language. Sbt as an example may analyze the changes to a changed file to decide whether depending classes need to be recompiled. So when the change only affect private methods, sbt can decide that dependencies need not be recompiled. However the benefit only becomes noticeable at large scales.

Caching influences incremental builds. Several buildsystems have a simple caching strategy in that they will not run a task if the output still exist. This will improve performance for repeated builds.

Some buildsystems can offer advanced (true) caching of build results, in that the cache is an independent storage that maintains multiple versions of build results over time. This can dramatically reduce build times in many more situations than simple caching described before.

For large projects with plenty of subprojects and subtasks, performance can be gained by caching in a fine-grained way and reusing more previous artifacts. The example and setup used in this benchmark may not be optimal for any given buildsystem. In particular, Pants has some online examples defining plenty of smaller library targets for individual Java files, which might improve caching performance when rebuilding after a single java file changed (not sure what other advantage it could have).

### I get InstantiationExceptions with some buildsystem, what is going on?

java.lang.InstantiationException during tests is usually a sign that a TestRunner is trying to run a non-Testcase class (like abstract or util classes). Not all Buildsystems can cope well with that by default.

### Why GNU make?

I chose GNU make for this project because it is omnipresent in linux and very close to shell scripting.

### Why jinja2?

I needed some templating engine, and scripting in Python seemed the least effort. Jinja2 is popular and een around for a while. Mako and Genshi also seemed nice at a glance.

### Why commons-math?

I chose to test against commons-math because it is reasonably large, well tested, and has no dependencies outside the JDK. Other libraries working okay are commons-text, commons-io, commons-imaging, guava.

The main problems I had with commons-math was that the naming for the Testcases is not consistent. The commons-math ant file lists those rules:

```xml
    <include name="**/*Test.java"/>
    <include name="**/*TestBinary.java"/>
    <include name="**/*TestPermutations.java"/>
    <exclude name="**/*AbstractTest.java"/>
```

And even those do not cover all Testcases defined in the codebase.

